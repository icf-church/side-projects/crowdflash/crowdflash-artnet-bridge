require('dotenv').config();

const dmxlib = require('dmxnet');
const redis = require('redis');

const redisClient = redis.createClient({
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT,
});

if (process.env.REDIS_PASSWORD !== undefined) {
  redisClient.auth(process.env.REDIS_PASSWORD);
}

const options = {
  verbose: 1, //Verbosity, default 0
  oem: 0, //OEM Code from artisticlicense, default to dmxnet OEM.
  sName: 'Crowdflash-ArtNet-Bridge', // 17 char long node description, default to "dmxnet"
  lName: 'Bridge for sending DMX values to a redis server', // 63 char long node description, default to "dmxnet - OpenSource ArtNet Transceiver"
  hosts: [process.env.ARTNET_IP], // Interfaces to listen to, all by default
};

const dmxnet = new dmxlib.dmxnet(options);

let color = {
  red: 0,
  green: 0,
  blue: 0,
};

let previousColors = {...color};
let textId = 0;
let previousTextId = 0;

let receiver = dmxnet.newReceiver({
  net: process.env.ARTNET_NETWORK, //Destination net, default 0
  subnet: process.env.ARTNET_SUBNET, //Destination subnet, default 0
  universe: process.env.ARTNET_UNIVERSE, //Destination universe, default 0
});

receiver.on('data', (data) => {
  color.red = data[0];
  color.green = data[1];
  color.blue = data[2];
  textId = data[3];
});

function sendColors() {
  if (color.red === previousColors.red && color.green === previousColors.green && color.blue === previousColors.blue && textId === previousTextId) {
    // Nothing has changed
    setTimeout(sendColors, process.env.SENDING_TIMEOUT);
    return;
  }

  // Publish color changes to redis
  redisClient.publish('colorUpdate', JSON.stringify({color, textId}));

  previousColors = {...color};
  previousTextId = textId;
  setTimeout(sendColors, process.env.SENDING_TIMEOUT);
}

sendColors();
