# Crowdflash

Crowdflash is an application that allows displaying colors on many smartphones and tablets at the same time, controlled via DMX interfaces.

Crowdflash consists of four main components that interact with one another in real time.

- Crowdflash Bridge
  - reads input via [ArtNet](https://gitlab.com/icf-church/side-projects/crowdflash/crowdflash-artnet-bridge) or [sACN](https://gitlab.com/icf-church/side-projects/crowdflash/crowdflash-sacn-bridge) and publishes updates to Redis
- Redis Pub/Sub Server
- [Crowdflash Websocket Server](https://gitlab.com/icf-church/side-projects/crowdflash/crowdflash-pubsub-server)
  - subscribes to Redis and pushes updates to all connected websocket clients
- [Crowdflash Client](https://gitlab.com/icf-church/side-projects/crowdflash/crowdflash-client)
  - displays the received colors on the screen

# Crowdflash ArtNet Bridge

The Crowdflash ArtNet Bridge is a simple ArtNet receiver that publishes DMX changes to a Redis server. Changes are saved instantly, but published only every x milliseconds to avoid flooding the Redis with too many requests.

Even though it should work with any device capable of sending ArtNet, the bridge is currently only tested with the following devices. I would be very interested in hearing from you if you're running it with other devices.

- GrandMA2 console
- [Art-Net Controller](https://play.google.com/store/apps/details?id=com.litux.art_net_controller) (Android app)

## Requirements

This app requires Node.js to run. It is tested with node v16.x, but should run with lower versions just fine.
The ArtNet receiver needs to be reachable from the network that the sending ArtNet device is in, while also having access to the Redis server which is running on the internet. For us it works great to run it on a VM within our event network, but running it on a laptop with WiFi or a Raspberry PI with internet connection should do the trick as well.

## Installation

Download the source via git or as a zip.
Install requirements by running

```bash
npm ci
```

## Usage

Running the app can be as simple as executing

```bash
node app.js
```

For a more permanent solution however consider using a process manager like [pm2](https://pm2.keymetrics.io/).

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/) - see [LICENSE](LICENSE) file
